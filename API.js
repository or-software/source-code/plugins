/**
 * title: API
 * version: 1.0.0-alpha
 */
export const API = {
    plugins: {},
    newEl: el => document.createElement(el),
    addRule(selector, rule) {    
        this.styleEl.sheet.insertRule(`${selector} {${rule}}`);
    },
    closeBtn() {
        let x = this.newEl('button');
        x.innerHTML = '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M20 6.91L17.09 4L12 9.09L6.91 4L4 6.91L9.09 12L4 17.09L6.91 20L12 14.91L17.09 20L20 17.09L14.91 12L20 6.91Z" /></svg>';
        x.classList.add('closeBtn');
        return x;
    },
    loadStyle(src) {
        return new Promise(function (resolve, reject) {
            let link = document.createElement('link');
            link.href = src;
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.onload = () => resolve(link);
            link.onerror = () => reject(new Error(`Style load error for ${src}`));
            document.head.append(link);
        });
    },
    call (cmd, ...args) {},
    Panel: class {
        panel = API.newEl("div");
        constructor(id) {
            this.panel.id = id;
            this.panel.classList.add('panel');
            document.body.append(this.panel);
        }
    },
    Tool: class {
        tool = API.newEl("button");
        menuDefaults = {
            "Move": ()=>{},
        };
        constructor(label="", panel=new HTMLElement(), icon=new HTMLElement()) {
            this.tool.title = label;
            tool.append(icon);
            panel.append(tool);
            this.tool.addEventListener("contextmenu", showMenu);
        }
        action(leftClickFunction = ()=>{}) {
            tool.addEventListener("click", leftClickFunction);
        }
        menu(options = {"Label":()=>{}}) {
            this.menuDefaults = {...this.menuDefaults, ...options};
        }
    },
    MenuItem: class {
        panel = API.menuPanel;
        wrapper = API.newEl('div');
        button = API.newEl('button');
        dropdown = API.newEl('div');
        listItems = {};
        constructor(label) {
            this.wrapper.classList.add('menuItem');
            this.panel.panel.append(this.wrapper);
            this.dropdown.classList.add('menuDropdown');
            this.wrapper.append(this.dropdown);
            this.button.innerText = label;
            this.button.classList.add('menuBtn');
            this.wrapper.append(this.button);
        }
        addListItem(name) {
            var listItem = API.newEl('div');
            listItem.innerText = name;
            listItem.classList.add('menuDropdownItem');
            this.dropdown.append(listItem);
            this.listItems[name] = listItem;
        }
    },
    TabbedMenu: class {
        button = API.newEl('button');
        section = API.newEl('div');
        constructor(label, panel) {
            this.button.innerText = label;
            panel.tabs.append(this.button);
            panel.sections.append(this.section);
        }
        addListItem(name) {
            var menuItem = API.newEl('div');
            menuItem.classList.add('menuPanelItem');
            this.section;
        }
    },
    Theme: class {
        constructor(name, vars) {
            this.vars = vars;
        }
    },
    PopUp: class {
        popup = API.newEl('div');
        constructor() {
            this.popup.classList.add('popup');
            this.hide();
            document.body.append(this.popup);
        }
        show() {
            this.popup.style.display = '';
        }
        hide() {
            this.popup.style.display = 'none';
        }
    }
};
API.styleEl = API.newEl('style');
API.TabbedPanel = class extends API.Panel {
    tabs = API.newEl('div');
    sections = API.newEl('div');
    constructor(id) {
        super(id);
        this.panel.append(this.tabs);
        this.panel.append(this.sections);
    }
};
document.head.appendChild(API.styleEl);
// API.plugins.API = { onload: () => {}};
