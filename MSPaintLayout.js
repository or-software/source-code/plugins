/**
 * title: BackgroundSetter
 * depends: API
 */
import './API.js';
API.tabbedPanel = new API.TabbedPanel("tools");
API.canvasPanel = new API.Panel("canvas");
API.statusPanel = new API.Panel("status");
API.addRule("body", `display: grid; grid-template-rows: 5% 91% 4%;`);
API.addRule("#tools", `grid-row: 1`);
API.addRule("#canvas", `grid-row: 2`);
API.addRule("#status", `grid-row: 3`);

API.menuItems = {
    file: new API.TabbedMenu('File', API.tabbedPanel),
    view: new API.TabbedMenu('View', API.tabbedPanel),
}
