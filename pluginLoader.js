const orSoft = true;
const loadedPlugins = {};
var path = '/plugins/';
if (document.location.href.match('netlify')) {
    path = 'https://or-plugins.netlify.app' + path;
}
async function loadPlugin(file) {
    return import(`${path}${file}.js`).then(plugin => {
        loadedPlugins[file] = plugin;
        window[file] = loadedPlugins[file][file];
    }).then(() => {
        let p = API.plugins[file];
        if (p) {
            if (p.onload) {
                p.onload();
            }
        }
    });
}
loadPlugin("API")
.then(() => fetch('/settings.json'))
.then(response => response.json())
.then(data => window.settings = data)
.then(() => loadPlugin(`${window.settings.layout}Layout`))
.then(() => API.loadStyle(`${path}${window.settings.theme}Theme.css`).catch(e => console.warn(e)))
.then(() => {
    for (let p of window.settings.plugins.enabled) {
        loadPlugin(p);
    }
}).then(() => {
    document.querySelector('.dots-wrapper').classList.add('hide');
});
