# Plugins
Each plugin must have front matter like this:
```
/**
 * title: PluginName
 * depends: dependecy1, dependecy2
 * version: 1.0.0
 */
```

Make sure dependecies are loaded using the import keyword:
```
import './API.js';
```

The version number ought to follow a 3-part Semantic Versioning scheme. The first part is the major version number, the second part is the minor version number, and the third part is the patch version number.

Plugin names ought to be camel cased, with the first letter capitalized; e.g "BackgroundSetter". All layout plugins must end with the word "Layout".

Theme plugins are written in CSS, file extension must be `.css`. All other plugins are written in JavaScript, file extension must be `.js`.
