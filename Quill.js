/**
 * title: Canvas
 * depends: API
 */
import './API.js';
import 'https://cdn.quilljs.com/1.3.6/quill.core.js';
// Syntax highlighting: https://highlightjs.readthedocs.io/en/latest/api.html#configure
API.loadStyle('https://cdn.quilljs.com/1.3.6/quill.core.css');
export var quill = {
    editor: API.addEl('div'),
    menuWrapper: API.addEl('div'),
    editorWrapper: API.addEl('div'),
}
document.body.append(quill.menuWrapper);
document.body.append(quill.editorWrapper);
quill.editorWrapper.append(quill.editor);
quill.quill = new Quill(quill.editor, {
    scrollingContainer: quill.editorWrapper,
});
