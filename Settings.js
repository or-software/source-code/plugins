/**
 * title: Settings
 * depends: API
 */
import './API.js';
export const Settings = new API.PopUp();
Settings.nav = API.newEl('div');
Settings.pages = API.newEl('div');
Settings.menus = {};
API.addRule('.settingsHidden', 'display: none');
Settings.nav.classList.add("settingsNav");
Settings.pages.classList.add("settingsPage");
Settings.closeBtn = API.closeBtn();
Settings.closeBtn.addEventListener('click', ()=>Settings.hide());
Settings.popup.append(Settings.closeBtn);
Settings.popup.append(Settings.nav);
Settings.popup.append(Settings.pages);
Settings.addPage = function(name) {
    Settings.menus[name] = API.newEl('div');
    Settings.menus[name].innerText = name;
    Settings.nav.append(Settings.menus[name]);
    Settings.menus[name].page = API.newEl('div');
    Settings.menus[name].page.id = `${name}Page`;
    Settings.menus[name].page.classList.add('settingsHidden');
    Settings.pages.append(Settings.menus[name].page);
    Settings.menus[name].addEventListener('click', () => {
        for (var menu in Settings.menus) {
            Settings.menus[menu].classList.remove('settingsSelectedMenu');
            Settings.menus[menu].page.classList.add('settingsHidden');
        }
        Settings.menus[name].classList.add('settingsSelectedMenu');
        Settings.menus[name].page.classList.remove('settingsHidden');
    });
}
Settings.addItem = function(page, item, setting) {
    var itemH3 = API.newEl('h3');
    itemH3.innerText = item;
    Settings.menus[page].page.append(itemH3);
    setting.forEach(s => {
        var div = API.newEl('div');
        div.classList.add(s.type);
        switch (s.type) {
            case 'singleSelectFlex':
                s.options.forEach((option, i) => s.handler(div, option, i)); break;
        }
        Settings.menus[page].page.append(div);
    });
}
Settings.saveSetting = function(key, value) {
    settings[key] = value;
}
Settings.addPage('Look');
API.menuItems.file.addListItem('Settings');
API.menuItems.file.listItems['Settings'].addEventListener('click', () => Settings.show());
