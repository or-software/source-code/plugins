/**
 * title: Canvas
 * depends: API
 */
import './API.js';
export class Canvas {
    panel = API.canvasPanel.panel;
    canvasWrapper = API.newEl("div");
    canvas = API.newEl('canvas');
    ctx;
    constructor (width, height, alpha) {
        this.canvasWrapper.id = "canvasWrapper";
        this.panel.append(this.canvasWrapper);
        this.canvas.width = width;
        this.canvas.height = height;
        this.ctx = this.canvas.getContext("2d", {alpha});
        this.canvasWrapper.append(this.canvas);
        API.addRule("#canvasWrapper", `overflow: scroll;
            max-height: 100%;
            max-width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;`);
        API.addRule("canvas", `background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='32' height='32' viewBox='0 0 8 8'%3E%3Cg fill='%23969696' fill-opacity='1'%3E%3Cpath fill-rule='evenodd' d='M0 0h4v4H0V0zm4 4h4v4H4V4z'/%3E%3C/g%3E%3C/svg%3E");`);
    }
}
API.plugins.Canvas = { onload: () => {
    API.canvas = new Canvas("1920", "1080", true);
}};
