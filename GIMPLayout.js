/**
 * title: GIMPLayout
 * depends: API
 */
import './API.js';
API.menuPanel = new API.Panel("menu");
API.toolsPanel = new API.Panel("tools");
API.canvasPanel = new API.Panel("canvas");
API.layersPanel = new API.Panel("layers");
API.statusPanel = new API.Panel("status");
API.addRule("body", `display: grid; grid-template-columns: 10% 80% 10%; grid-template-rows: min-content auto min-content;`);
API.addRule('.panel', 'min-height: 20px; min-width: 20px;');
API.addRule("#menu", `grid-row: 1; grid-column: 1 / 4;`);
API.addRule("#tools", `grid-row: 2; grid-column: 1;`);
API.addRule("#layers", `grid-row: 2; grid-column: 3;`);
API.addRule("#canvas", `grid-row: 2; grid-column: 2;`);
API.addRule("#status", `grid-row: 3; grid-column: 1 / 4;`);

API.menuItems = {
    file: new API.MenuItem('File'),
    edit: new API.MenuItem('Edit'),
    view: new API.MenuItem('View'),
    help: new API.MenuItem('Help'),
}
